/* eslint-disable no-plusplus */
// --------------------------------------IMPORTS------------------------------------
// ---Dependencies
import express, { Request, Response } from 'express';
// ---Model Data
import {
  DBPersona, PersonaWithID, Persona, validateAddData, validatePutData
} from '#DataModel/Crud';
// ---Custom
import { responseService, joiValidateService } from '#Config/respondServices';
import { Callback } from '#Config/customTypes';

// -----------------------------------CONFIG-------------------------------
const router = express.Router();

// -----------------------------------ROUTES-------------------------------
// ---- Create ---------
// ---- GET ---------
router.get('/Get', (req: Request, res: Response) => {
  console.log(`request for: ${req.originalUrl}`);
  responseService(res, getPerson as Callback, null);
});
// ---- POST ---------
router.post('/Add', (req: Request, res: Response) => {
  console.log(`request for: ${req.originalUrl}`);
  const validateBody = validateAddData(req.body);
  if (joiValidateService(res, validateBody)) {
    responseService(res, addPerson as Callback, req.body);
  }
});
// // ---- PUT ---------
router.put('/Put', (req: Request, res: Response) => {
  console.log(`request for: ${req.originalUrl}`);
  const validateBody = validatePutData(req.body);
  if (joiValidateService(res, validateBody)) {
    responseService(res, putPerson as unknown as Callback, req.body);
  }
});
// ---- DELETE ---------
router.delete('/Delete/:_id', (req: Request, res: Response) => {
  console.log(`request for: ${req.originalUrl}`);
  const validateBody = validatePutData({ _id: req.params._id });
  if (joiValidateService(res, validateBody)) {
    responseService(res, deletePerson as unknown as Callback, req.params._id);
  }
});
// --------------------------------- QUERYS AND METHODS --------------------------
async function putPerson(putData: PersonaWithID) {
  try {
    const searchedPerson = await DBPersona.findById(putData._id);
    searchedPerson.set({ ...searchedPerson, ...putData });
    const promiseResponse = await searchedPerson.save();
    return {
      internalError: false,
      result: { status: 'success', data: promiseResponse }
    };
  } catch (error) {
    return {
      internalError: true,
      result: { error, errorType: 'Error al editar en DB' }
    };
  }
}
async function deletePerson(_id: string) {
  try {
    const deletedPerson = await DBPersona.findByIdAndDelete(_id);
    return {
      internalError: false,
      result: { status: 'success', data: deletedPerson }
    };
  } catch (error) {
    return {
      internalError: true,
      result: { error, errorType: 'Error al eliminar en DB' }
    };
  }
}

async function addPerson(data: Persona) {
  const dataNueva = new DBPersona(data);
  try {
    const promiseResponse = await dataNueva.save();
    return {
      internalError: false,
      result: { status: 'success', data: promiseResponse }
    };
  } catch (error) {
    return {
      internalError: true,
      result: { error, errorType: 'Error al registrar en DB', statusError: 401 }
    };
  }
}

async function getPerson() {
  try {
    const allPersons = await DBPersona.find();
    return {
      internalError: false,
      result: { status: 'success', data: allPersons }
    };
  } catch (error) {
    return {
      internalError: true,
      result: { error, errorType: 'Error al consultar DB' }
    };
  }
}

export default router;
