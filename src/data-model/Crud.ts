// --------------------------------------IMPORTS------------------------------------
// Dependencies
import Joi from 'joi';
import mongoose from 'mongoose';
// --------------------------MODEL DATA DB -----------------------
const dbPersonaSchema = new mongoose.Schema({
  name: { type: String, required: true },
  lastName: { type: String, required: true },
  age: { type: Number, required: true },
  edit: Boolean
});

export const DBPersona = mongoose.model('Persona', dbPersonaSchema);
// --------------------------MODEL DATA JOI VALIDATORS-----------------------
export function validateAddData(data: unknown): Joi.ValidationResult {
  const schema = Joi.object({
    name: Joi.string().required(),
    lastName: Joi.string().required(),
    age: Joi.number().required(),
    edit: Joi.boolean()
  });
  return schema.validate(data);
}

export function validatePutData(data: unknown): Joi.ValidationResult {
  const schema = Joi.object({
    _id: Joi.string().required().pattern(/^[0-9a-fA-F]{24}$/),
    name: Joi.string(),
    lastName: Joi.string(),
    age: Joi.number(),
    edit: Joi.boolean()
  });
  return schema.validate(data);
}

// ----------------------------- TS TYPE ---------------------------
export interface Persona {
  name: string;
  lastName: string;
  age: number;
  edit:boolean
}

export interface PersonaWithID {
  _id: string;
  name?: string;
  lastName?: string;
  age?: number;
  edit?:boolean
}
