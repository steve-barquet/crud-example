import mongoose from 'mongoose';

export default async function mongoConnect() {
  // Mongo conect to base
  try {
    const mongoUri = 'mongodb://127.0.0.1:27017/crud-example';
    console.log(`mongo url: ${mongoUri}`);
    await mongoose.connect(mongoUri); // Return a promise
    console.log(`Conected to ${mongoUri}...`);
  } catch (err) {
    console.log('Couldnt connect because:\n', err);
  } finally {
    console.log('Connection try finished...');
  }
}
